class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  before_action :current_admin
  helper_method :current_admin, :admin_login?

 def current_admin
@current_admin ||= Admin.find(session[:admin_id]) if session[:admin_id]
 end
  def admin_loggedin?
   !!@current_admin

  end
  def authorize

    if (request.path_parameters[:format] != 'json')

        redirect_to signin_path  if !admin_loggedin?
    end

  end



end
