class CompaniesController < ApplicationController
  before_action :authorize
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  layout :false, except: [:edit,:show]
  layout "root", except: [:edit,:show]

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    randomstring = (0...10).map { o[rand(o.length)] }.join
    @company = Company.new(company_params)
    @company.token=randomstring
    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Company was successfully updated.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def alldocuments 



render json: JSON.generate(alldocs)

  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:company_name)
    end



    def alldocs  


    alldoc=Document.where(company_id: params[:company_id]).order(:document_position).as_json

    allfolder=Folder.where(company_id: params[:company_id]).order(:folders_position).as_json

    alldoc.each_with_index do |document,index|

      unless document["folder_id"].nil?

        alldoc[index][:url]=company_folder_document_url(params[:company_id],document["folder_id"],document["id"],format: :json)
        alldoc[index][:folder_url]=company_folder_documents_url(params[:company_id],document["folder_id"],format: :json)

      else

        alldoc[index][:url]=company_document_url(params[:company_id],document["id"],format: :json)
        alldoc[index][:folder_url]=company_documents_url(params[:company_id],format: :json)
      end


    end

    allfolder.each_with_index do |folder,index|
      allfolder[index][:url]=company_folder_documents_url(params[:company_id],folder["id"], format: :json)

      unless folder["folder_id"].nil?


        allfolder[index][:parrent_folder_url]=company_folder_documents_url(params[:company_id],folder["folder_id"],format: :json)

      else


        allfolder[index][:parrent_folder_url]=company_documents_url(params[:company_id],format: :json)
      end



    end

    procceddata=Hash.new 

    procceddata["documents"]=alldoc.select { |a| a["folder_id"].nil? }  
    procceddata["subfolders"]=procedure alldoc,allfolder
  
      return procceddata

    end





    def procedure documents,folders,parent=nil

   
  pfolders=folders.select { |a| a["folder_id"]==parent }  
  subfolders=folders.reject { |a| a["folder_id"]==parent }

  

    pfolders.each_with_index  do |pfolder, index| 
    pfolders[index]["documents"]=documents.select { |a| a["folder_id"]==pfolder["id"]}
    documents=folders.reject { |a| a["folder_id"]==pfolder["id"] }
    pfolders[index]["subfolders"]=procedure(documents,subfolders,pfolder["id"])


  end

  return pfolders
  

  end


end
