class DocumentsController < ApplicationController
  before_action :authorize
  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_action :set_company, only: [:index, :new, :create, :show, :edit, :update, :destroy, :newfolder, :createfolder ,:destroyfolder, :editfolder, :updatefolder]
  before_action :set_current_folder, only: [:index, :new, :create, :show, :edit, :update, :destroy , :newfolder, :createfolder, :destroyfolder , :editfolder, :updatefolder]
   before_action :set_folder, only: [:destroyfolder, :editfolder, :updatefolder]
  # GET /documents
  # GET /documents.json
  def index
    if params.has_key?(:folder_id)
      
       
    @folders=@company.folders.where(folder_id: @current_folder.id ).order(:folders_position)
    @documents = @company.documents.where(folder_id: @current_folder.id).order(:document_position)
   
    else 

    @folders=@company.folders.where("folder_id IS NULL").order(:folders_position)
    @documents = @company.documents.where("folder_id IS NULL").order(:document_position)
   

    end

  if (params["format"]=="json")
      dochas=@documents.as_json
      folderhas=@folders.as_json

      dochas.each_with_index do |document,index|

        unless document["folder_id"].nil?

          dochas[index][:url]=company_folder_document_url(@company,document["folder_id"],document["id"],format: :json)
          dochas[index][:folder_url]=company_folder_documents_url(@company,document["folder_id"],format: :json)

        else

          dochas[index][:url]=company_document_url(@company,document["id"],format: :json)
          dochas[index][:folder_url]=company_documents_url(@company,format: :json)
        end


      end

      folderhas.each_with_index do |folder,index|
        folderhas[index][:url]=company_folder_documents_url(@company,folder["id"], format: :json)

        unless folder["folder_id"].nil?


          folderhas[index][:parrent_folder_url]=company_folder_documents_url(@company,folder["folder_id"],format: :json)

        else


          folderhas[index][:parrent_folder_url]=company_documents_url(@company,format: :json)
        end



      end

      procceddata=Hash.new
      if @current_folder
      unless @current_folder.folder_id.nil?
        procceddata["parent_folder_url"]=company_folder_documents_url(@company,@current_folder.folder_id, format: :json)

      else
        procceddata["parent_folder_url"]=company_documents_url(@company, format: :json)
      end
      end


    procceddata["documents"]=dochas
    procceddata["subfolders"]=folderhas

    render json: JSON.generate(procceddata)
  end
    
  end

  # GET /documents/1
  # GET /documents/1.json
  def show

    
  end

  def sortfolder
  pos=1

  params[:sortfolder].each do |foldersort|
  folder=Folder.find(foldersort) 
  folder.folders_position=pos
  folder.save
  pos+=1
    end  
  
  render plain: "successfully sorted"

  end

  def sortfile
 pos=1

  params[:sortdoc].each do |docsort|
  document=Document.find(docsort) 
  document.document_position=pos
  document.save
  pos+=1
    end  
  
  render plain: "successfully sorted file"
  end

  # GET /documents/newd
  def new
    @document = Document.new
  end

   def newfolder
    @folder = Folder.new
  end

def createfolder
    @folder = Folder.new(folder_params)
    @folder.company=@company 
    if params.has_key?(:folder_id)

    @folder.folder=@current_folder 
   
    end 

     if params.has_key?(:folder_id)

    respond_to do |format|
      if @folder.save
        format.html { redirect_to company_folder_documents_path(@company,@current_folder), notice: 'Document was successfully created.' }
      
      else
        format.html { render :new }
      end
    end
  
      else

    respond_to do |format|
      if @folder.save
        format.html { redirect_to company_documents_path(@company), notice: 'Document was successfully created.' }
      
      else
        format.html { render :new }
      end
    end
  end


  end

  # GET /documents/1/edit
  def edit
  end

  def editfolder
  end

  # POST /documents
  # POST /documents.json
  def create
    @file=params[:document_link];
    @document = Document.new(document_params)
    @document.company=@company 
    if params.has_key?(:folder_id)

    @document.folder_id=@current_folder.id 
   
    end 

  

    respond_to do |format|
      if @document.save
            if params.has_key?(:folder_id)

    
      format.html { redirect_to [@company,@current_folder,:documents], notice: 'Document was successfully created.' }

      format.json { render :show, status: :created, location: [@company,@current_folder,:documents] }
          else 

      format.html { redirect_to [@company,:documents], notice: 'Document was successfully created.' }
      format.json { render :show, status: :created, location: [@company,:documents] }

            end

      
      else
        format.html { render :new }
        format.json { render json: [@company, @document].errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update

    respond_to do |format|
      if @document.update(document_params)
         if params.has_key?(:folder_id)

    
      format.html { redirect_to [@company,@current_folder,:documents], notice: 'Document was successfully updated.' }
      format.json { render :show, status: :ok, location: [@company,@current_folder,:documents] }

         else

      format.html { redirect_to [@company,:documents], notice: 'Document was successfully updated.' }
      format.json { render :show, status: :ok, location: [@company,:documents] }

         end

      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

    def updatefolder
    respond_to do |format|
      if @folder.update(folder_params)
         if params.has_key?(:folder_id)

    
      format.html { redirect_to [@company,@current_folder,:documents], notice: 'Document was successfully updated.' }
          else 

      format.html { redirect_to [@company,:documents], notice: 'Document was successfully updated.' }
   
           end 

        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @folder.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /documents/1
  # DELETE /documents/1.json'

  def destroy
    @document.destroy
      respond_to do |format|
           if params.has_key?(:folder_id)

    
      format.html { redirect_to company_folder_documents_url(@company,@current_folder) , notice: 'Document was successfully updated.' }
          else 

      format.html { redirect_to company_documents_url(@company), notice: 'Document was successfully updated.' }
   
           end 
    end
  end

   def destroyfolder
    @folder.destroy
    respond_to do |format|
        if params.has_key?(:folder_id)

    
      format.html { redirect_to company_folder_documents_url(@company,@current_folder) , notice: 'Document was successfully updated.' }
      format.json { render :show, status: :created, location: company_folder_documents_url(@company,@current_folder) }


        else

      format.html { redirect_to company_documents_url(@company), notice: 'Document was successfully updated.' }
      format.json { render :show, status: :created, location: company_documents_url(@company) }

        end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end
      def set_folder
      @folder = Folder.find(params[:id])
    end
     def set_company
      @company = Company.find(params[:company_id])
    end

    def set_current_folder
          if params[:folder_id].present?
    
      @current_folder = Folder.find(params[:folder_id])

           end
    end

   def folder_params
      params.require(:folder).permit(:company_id, :folder_id, :folder_name )
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:company_id, :folder_id, :document_name, :document_link , :document_description)
    end









end
