class SessionController < ApplicationController

  layout  "signin"

  def new

  end
  def create
    admin= Admin.find_by(admin_email: params[:session][:admin_email].downcase)
    if admin && admin.authenticate(params[:session][:password])
      session[:admin_id]=admin.id
      redirect_to companies_path, notice: 'Company was successfully signin.'
    else
        flash.now[:danger]="there is some problem"

      render :new
    end

  end

  def destroy
     session[:admin_id]=nil
    redirect_to signin_path
  end

end
