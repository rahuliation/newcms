class Admin < ApplicationRecord
  validates :admin_fullname, :admin_email, presence: true
  validates :admin_email, uniqueness: true, on: :create
  validates_format_of :admin_email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  has_secure_password

end
