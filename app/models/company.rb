class Company < ApplicationRecord
	has_many :folders
	has_many :documents
	has_many :contact_persons
	has_many :company_infos
	validates :company_name, presence: true


end
