class ContactPerson < ApplicationRecord

  belongs_to :company
  validates :contact_name, presence: true

end
