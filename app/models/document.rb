class Document < ApplicationRecord

	belongs_to :folders , optional:true 
	belongs_to :company

	mount_uploader :document_link, DocumentUploader
	validates_presence_of :document_link


end