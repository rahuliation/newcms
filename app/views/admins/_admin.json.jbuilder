json.extract! admin, :id, :admin_fullname, :admin_email, :created_at, :updated_at
json.url admin_url(admin, format: :json)