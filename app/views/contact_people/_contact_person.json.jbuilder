json.extract! contact_person, :id, :contact_name, :contact_title, :contact_phone, :contact_email, :created_at, :updated_at
json.url company_contact_person_url(@company,contact_person, format: :json)