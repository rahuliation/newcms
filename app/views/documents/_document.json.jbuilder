json.extract! document, :id, :document_name, :document_description , :document_size, :document_type, :document_link ,:created_at, :updated_at
if @current_folder
json.url company_folder_document_url(@company,@current_folder,document, format: :json)
json.folder_url company_folder_documents_url(@company,@current_folder, format: :json)
else

  json.url company_document_url(@company,document, format: :json)
  json.folder_url company_documents_url(@company, format: :json)

end