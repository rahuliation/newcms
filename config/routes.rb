Rails.application.routes.draw do
  resources :admins
  resources :companies do

    get :alldocuments, to: 'companies#alldocuments'


    resources :folders do
      resources :documents do
        collection do
          get :newfolder , to: 'documents#newfolder'
          post :newfolder , to: 'documents#createfolder'
          delete 'newfolder/:id(.:format)', to: 'documents#destroyfolder'
          get 'newfolder/:id(.:format)/edit', to: 'documents#editfolder'
          match 'newfolder/:id(.:format)', :via => :put, :to => 'documents#updatefolder'
          match 'newfolder/:id(.:format)', :via => :patch,:to => 'documents#updatefolder'
          post :sortfile
          post :sortfolder
        end
      end

    end

    resources :documents do
      collection do
        get :newfolder
        post 'newfolder', to: 'documents#createfolder'
        delete 'newfolder/:id(.:format)', to: 'documents#destroyfolder'
        get 'newfolder/:id(.:format)/edit', to: 'documents#editfolder'
        put 'newfolder/:id(.:format)', to: 'documents#updatefolder'
        patch 'newfolder/:id(.:format)', to: 'documents#updatefolder'
        post :sortfile
        post :sortfolder
      end
    end
    resources :company_infos
    resources :contact_people

  end

  get '' , to: 'companies#index'
  get 'signin' , to: 'session#new'
  post 'signin' , to: 'session#create'
  delete 'signout' , to: 'session#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


end
