class CreateFolders < ActiveRecord::Migration[5.0]
  def change
    create_table :folders do |t|
      t.string :folder_name
      t.integer :folders_position
      t.references :folder
      t.references :company


      t.timestamps
    end
  end
end
