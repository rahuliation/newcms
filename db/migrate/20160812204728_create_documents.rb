class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|
    	t.string :document_name
    	t.text :document_description
    	t.string :document_type
    	t.string :document_size
    	t.references :folder

      	t.timestamps
    end
  end
end
