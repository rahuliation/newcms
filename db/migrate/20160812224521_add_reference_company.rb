class AddReferenceCompany < ActiveRecord::Migration[5.0]
  def change
  	add_reference(:documents, :company , index: true)
  end
end
