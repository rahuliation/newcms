class AddPositionAndLinkColumnDocument < ActiveRecord::Migration[5.0]
  def change
  	add_column(:documents ,:document_position ,:integer, after: :document_description)
  	add_column(:documents ,:document_link ,:integer, after: :document_description)

  end
end
