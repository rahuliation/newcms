class ChangeTypeDocumentLinkOfDocumentTable < ActiveRecord::Migration[5.0]
  def change

  	    change_column :documents, :document_link, :string

  end
end
