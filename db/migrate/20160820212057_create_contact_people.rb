class CreateContactPeople < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_people do |t|
      t.string :contact_name
      t.string :contact_title
      t.string :contact_phone
      t.string :contact_email

      t.timestamps
    end
  end
end
