class CreateCompanyInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :company_infos do |t|
      t.string :company_title
      t.text :company_description
      t.string :company_url
      t.string :company_phone
      t.string :company_address_one
      t.string :company_address_two
      t.string :company_city
      t.string :company_state
      t.string :company_zipcode
      t.string :company_country

      t.timestamps
    end
  end
end
