class AddCompanyReferenceToContact < ActiveRecord::Migration[5.0]
  def change
    add_reference(:contact_people, :company , index: true)
    add_reference(:company_infos, :company , index: true)
  end
end

