class CreateTokenColumnToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column(:companies ,:token ,:string, after: :company_name)

  end
end
