class AddColumnPasswordDigest < ActiveRecord::Migration[5.0]
  def change
    add_column(:admins ,:password_digest ,:string, after: :admin_fullname)

  end
end
