# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160821233454) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "admin_fullname"
    t.string   "admin_email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "company_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "token"
  end

  create_table "company_infos", force: :cascade do |t|
    t.string   "company_title"
    t.text     "company_description"
    t.string   "company_url"
    t.string   "company_phone"
    t.string   "company_address_one"
    t.string   "company_address_two"
    t.string   "company_city"
    t.string   "company_state"
    t.string   "company_zipcode"
    t.string   "company_country"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "company_id"
    t.index ["company_id"], name: "index_company_infos_on_company_id", using: :btree
  end

  create_table "contact_people", force: :cascade do |t|
    t.string   "contact_name"
    t.string   "contact_title"
    t.string   "contact_phone"
    t.string   "contact_email"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "company_id"
    t.index ["company_id"], name: "index_contact_people_on_company_id", using: :btree
  end

  create_table "documents", force: :cascade do |t|
    t.string   "document_name"
    t.text     "document_description"
    t.string   "document_type"
    t.string   "document_size"
    t.integer  "folder_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "company_id"
    t.integer  "document_position"
    t.string   "document_link"
    t.index ["company_id"], name: "index_documents_on_company_id", using: :btree
    t.index ["folder_id"], name: "index_documents_on_folder_id", using: :btree
  end

  create_table "folders", force: :cascade do |t|
    t.string   "folder_name"
    t.integer  "folders_position"
    t.integer  "folder_id"
    t.integer  "company_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["company_id"], name: "index_folders_on_company_id", using: :btree
    t.index ["folder_id"], name: "index_folders_on_folder_id", using: :btree
  end

end
