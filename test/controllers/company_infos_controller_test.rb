require 'test_helper'

class CompanyInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @company_info = company_infos(:one)
  end

  test "should get index" do
    get company_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_company_info_url
    assert_response :success
  end

  test "should create company_info" do
    assert_difference('CompanyInfo.count') do
      post company_infos_url, params: { company_info: { company_address_one: @company_info.company_address_one, company_address_two: @company_info.company_address_two, company_city: @company_info.company_city, company_country: @company_info.company_country, company_description: @company_info.company_description, company_phone: @company_info.company_phone, company_state: @company_info.company_state, company_title: @company_info.company_title, company_ur: @company_info.company_ur, company_zipcode: @company_info.company_zipcode } }
    end

    assert_redirected_to company_info_url(CompanyInfo.last)
  end

  test "should show company_info" do
    get company_info_url(@company_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_company_info_url(@company_info)
    assert_response :success
  end

  test "should update company_info" do
    patch company_info_url(@company_info), params: { company_info: { company_address_one: @company_info.company_address_one, company_address_two: @company_info.company_address_two, company_city: @company_info.company_city, company_country: @company_info.company_country, company_description: @company_info.company_description, company_phone: @company_info.company_phone, company_state: @company_info.company_state, company_title: @company_info.company_title, company_ur: @company_info.company_ur, company_zipcode: @company_info.company_zipcode } }
    assert_redirected_to company_info_url(@company_info)
  end

  test "should destroy company_info" do
    assert_difference('CompanyInfo.count', -1) do
      delete company_info_url(@company_info)
    end

    assert_redirected_to company_infos_url
  end
end
