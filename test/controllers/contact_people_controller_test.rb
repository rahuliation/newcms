require 'test_helper'

class ContactPeopleControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contact_person = contact_people(:one)
  end

  test "should get index" do
    get contact_people_url
    assert_response :success
  end

  test "should get new" do
    get new_contact_person_url
    assert_response :success
  end

  test "should create contact_person" do
    assert_difference('ContactPerson.count') do
      post contact_people_url, params: { contact_person: { contact_email: @contact_person.contact_email, contact_name: @contact_person.contact_name, contact_phone: @contact_person.contact_phone, contact_title: @contact_person.contact_title } }
    end

    assert_redirected_to contact_person_url(ContactPerson.last)
  end

  test "should show contact_person" do
    get contact_person_url(@contact_person)
    assert_response :success
  end

  test "should get edit" do
    get edit_contact_person_url(@contact_person)
    assert_response :success
  end

  test "should update contact_person" do
    patch contact_person_url(@contact_person), params: { contact_person: { contact_email: @contact_person.contact_email, contact_name: @contact_person.contact_name, contact_phone: @contact_person.contact_phone, contact_title: @contact_person.contact_title } }
    assert_redirected_to contact_person_url(@contact_person)
  end

  test "should destroy contact_person" do
    assert_difference('ContactPerson.count', -1) do
      delete contact_person_url(@contact_person)
    end

    assert_redirected_to contact_people_url
  end
end
